var opened = [];
var last_reaction='';

function react(reagents){

    r_formula = reagents[0]+'+'+reagents[1];
    r_reversed_formula = reagents[1]+'+'+reagents[0];

	if(reactions[r_formula])
	 {
	    return reactions[r_formula];
	 }
	else
	 {
    	if(reactions[r_reversed_formula])
    	{
    	  return reactions[r_reversed_formula];
    	}
	 }

    r_formula = reagents[0]+'+*';
    r_reversed_formula = reagents[1]+'+*';

	if(reactions[r_formula])
	 {
	    return reactions[r_formula];
	 }
	else
	 {
    	if(reactions[r_reversed_formula])
    	{
    	  return reactions[r_reversed_formula];
    	}
	 }

	return 0;

}

function destroyElement(element){
	element.fadeOut(600, function(){element.remove()});
}

function clearBoard(){
	$('#board').children('.element').animate(
		{"top": $('#abyss').offset().top+50, "left": $('#abyss').offset().left+50}, 1000,
		function(){destroyElement($(this))}
	);
	//destroyElement($('#board').children('.element'));
}

function cloneElement(element){
	placeElements([element.text(), element.text()], element.offset());
	destroyElement(element);
}

function onDrop(event, ui) {
	var result = react([ui.draggable.text(), $(this).text()]);
	var result_str;
	if(result == 0)
		result_str = 'нет реакции'
	else{
		result_str = result;
		last_reaction = ui.draggable.text()+' + '+$(this).text()+' = '+result_str;
	}
	$('#info').html(last_reaction+'<br>');
	if(result!=0){
		placeElements(result, $(this).offset());
		destroyElement(ui.draggable);
		destroyElement($(this));
	}
	else result='no reaction';
}

function message(name){
	if(messages[name]!=undefined)
		$('#info').append('<span class="questbox">'+messages[name]+'</span>');
}

function refreshStat(){
	$('#stat').html('('+opened.length+'/'+(element_count-1)+')');
}

function dragStart(el){
	var newEl = el.cloneNode(true);
	el.parentNode.insertBefore(newEl, el);
	$(newEl).draggable({start: dragStart});
};

function discoverElement(name, verbose){
	if(!in_array(name, opened)){
		opened.push(name);
		if(verbose==undefined) message(name);
		var o = $('<span class="element '+styles[name]+'" title="'+last_reaction+'">'+name+'</span>').appendTo($('#stack'));

		o.bind("mousedown", function(e){addElement(name, o.offset()); e.stopPropagation()});
		o.css('position','relative');
		o.css('padding','0px');
		o.css('border','0px');
		$('#stack').append(' ');
		refreshStat();
		$('#save').show();
	}
}

function addElement(name, place){
	var a = $('<div class="element '+styles[name]+'">'+name+'</div>').appendTo('#board');
	if(place!=undefined){

		//a.offset({top: place.top+$(window).scrollTop(), left: place.left+$(window).scrollLeft()});
		a.animate({"top": place.top+$(window).scrollTop(), 'left': place.left+$(window).scrollLeft()},0);
	}
	a.draggable();
	a.droppable({drop: onDrop});
	a.bind("dblclick", function(e){cloneElement(a); e.stopPropagation();});
	discoverElement(name)
	return a;
}

function placeElements(names, place){
	var x = place.left, y = place.top;
	var c = names.length;
	var pi = Math.PI, a = 2*pi/c;
	var top, left, radius=20, start_angle=Math.random()*2*pi;
	var e;

	for(i in names){
		top = Math.floor((c-1)*radius*Math.sin(start_angle+i*a));
		left = Math.floor((c-1)*radius*Math.cos(start_angle+i*a));
		top<0 ? top = "-="+(-top)+"px" : top = "+="+top+"px";
		left<0 ? left = "-="+(-left)+"px" : left = "+="+left+"px";
		e = addElement(names[i], {"top":y, "left":x});
		e.css('opacity','0');
		e.animate({"top": top, "left": left, opacity:1}, 600);
	}
}

function in_array(what, where) {
	for(var i=0; i<where.length; i++)
		if(what == where[i])
			return true;
    return false;
}

function test(type){
	var result = [];
	var elements=inits.slice();
	for(var i in reactions){
		for(var j in reactions[i]){
			if(!in_array(reactions[i][j], elements))
				elements.push(reactions[i][j]);
		}
	}
	if(type=='total') return elements;
	if(type==undefined ||  type=='unstyled'){
		var unstyled = [];
		for (var i in elements){
			if( styles[elements[i]] == undefined)
				unstyled.push(elements[i]);
		}
	}
	if(type=='unstyled') return unstyled;
	result['total'] = elements;
	result['unstyled'] = unstyled;
	return result;

}

function save(){
	$.cookie('game',$.base64Encode(opened.join(';')),{expires:30});
	$('#save').fadeOut();
}

function load(){
	var a = $.base64Decode($.cookie('game')).split(';');
	$('#info').html('');
	$('.element').remove();
	$('#stack').show();
	opened = [];
	for(i in a) discoverElement(a[i], false);
	refreshStat();
}

var element_count;

$(function() {
	placeElements(inits,{top:300, left:400});
	$(document).bind("dblclick", function(e){
			placeElements(inits,{top:e.pageY, left:e.pageX});e.stopPropagation()});
	$("#abyss").droppable({drop: function(e, ui){destroyElement(ui.draggable)}});
	$('#stack-btn').hide();
	element_count = test('total').length;
	refreshStat();
});